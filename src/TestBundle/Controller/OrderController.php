<?php

namespace TestBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use APY\DataGridBundle\Grid\Source\Entity;
use TestBundle\Entity\Orders;
use TestBundle\Entity\OrderLine;
use TestBundle\Entity\Product;
use TestBundle\Entity\Address;
use TestBundle\Form\OrdersType;


class OrderController extends Controller
{
    public function saveAction()
    {
        $data = $this->get('lengow_test')->getOrdersFromXml();
        $em = $this->getDoctrine()->getManager();

        foreach($data as $key => $info) {
            $order = $em->getRepository('TestBundle:Orders')
                        ->findBy(array('reference' => $info['info']['reference']));
            if($order == null) {
                $order = new Orders;
                $order->setReference($info['info']['reference']);
                $order->setState($info['info']['state']);
                if(isset($info['info']['created'])) {
                    $order->setCreateDate($info['info']['created']);
                }
                $order->setAmount($info['info']['amount']);
                $order->setTax($info['info']['tax']);
                $order->setShipping($info['info']['shipping']);
                $order->setCommission($info['info']['commission']);
                $order->setCurrency($info['info']['currency']);
                $em->persist($order);
                $em->flush();
                $billing = new Address;
                $billing->setType('Billing');
                $billing->setOrders($order);
                $billing->setSociety($info['billing']['society']);
                $billing->setCivility($info['billing']['civility']);
                $billing->setFirstname($info['billing']['firstname']);
                $billing->setLastname($info['billing']['lastname']);
                $billing->setEmail($info['billing']['email']);
                $billing->setLocated($info['billing']['located']);
                $billing->setComplement($info['billing']['complement']);
                $billing->setZipcode($info['billing']['zipcode']);
                $billing->setCity($info['billing']['city']);
                $billing->setCountry($info['billing']['country']);
                $delivery = new Address;
                $delivery->setType('Delivery');
                $delivery->setOrders($order);
                $delivery->setSociety($info['delivery']['society']);
                $delivery->setCivility($info['delivery']['civility']);
                $delivery->setFirstname($info['delivery']['firstname']);
                $delivery->setLastname($info['delivery']['lastname']);
                $delivery->setEmail($info['delivery']['email']);
                $delivery->setLocated($info['delivery']['located']);
                $delivery->setComplement($info['delivery']['complement']);
                $delivery->setZipcode($info['delivery']['zipcode']);
                $delivery->setCity($info['delivery']['city']);
                $delivery->setCountry($info['delivery']['country']);
                $order->addAddress($billing);
                $order->addAddress($delivery);
                foreach($info['orderLines'] as $orderlineData) {
                    $product = $em->getRepository('TestBundle:Product')
                            ->findOneBy(array('sku' => $orderlineData['product']['sku']));
                    if($product == null) {
                        $product = new Product;
                        $product->setTitle($orderlineData['product']['title']);
                        $product->setEan($orderlineData['product']['ean']);
                        $product->setSku($orderlineData['product']['sku']);
                        $product->setImage($orderlineData['product']['image']);
                        $em->persist($product);
                        $em->flush();
                    }

                    $orderLine = new OrderLine;
                    $orderLine->setProduct($product);
                    $orderLine->setOrders($order);
                    $orderLine->setQuantity($orderlineData['orderLine']['quantity']);
                    $orderLine->setPrice($orderlineData['orderLine']['price']);
                    $orderLine->setUnitPrice($orderlineData['orderLine']['unitPrice']);
                    $orderLine->setShippingPrice($orderlineData['orderLine']['shippingPrice']);
                    $orderLine->setTax($orderlineData['orderLine']['tax']);
                    $order->addOrderLine($orderLine);
                }
                $em->flush();
            }
        }

        return $this->render('TestBundle:Order:save.html.twig');
    }

    public function listAction()
    {
        $source = new Entity('TestBundle:Orders');
        // Get a grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Manage the grid redirection, exports and the response of the controller
        return $grid->getGridResponse('TestBundle:Order:grid.html.twig');
    }

    public function apiAction($format)
    {
        $orders = $this->getDoctrine()->getManager()
                    ->getRepository('TestBundle:Orders')
                    ->findAll();

        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes(array('orderLines', 'address'));
        $encoder = new JsonEncoder();
        //$serializer = new Serializer(array($normalizer), array($encoder));

        if($format == "json") {
            $serializer = new Serializer(array($normalizer), array($encoder));
            $jsonContent = $serializer->serialize($orders, 'json');
        }
        else {
            $serializer = new Serializer(array($normalizer));

            $jsonContent = Yaml::dump($serializer->normalize($orders));
        }

        return new Response($jsonContent);
    }

    public function showAction($id_order)
    {
        $order = $this  ->getDoctrine()->getManager()
                        ->getRepository('TestBundle:Orders')
                        ->findById($id_order);

        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes(array('orderLines', 'address'));
        $encoder = new JsonEncoder();

        $serializer = new Serializer(array($normalizer), array($encoder));

        $jsonContent = $serializer->serialize($order, 'json');

        return new Response($jsonContent);
    }

    public function addAction()
    {
        $order = new Orders;
        $billing = new Address;
        $billing->setType('Billing');
        $billing->setOrders($order);
        $delivery = new Address;
        $delivery->setType('Delivery');
        $delivery->setOrders($order);
        $order->addAddress($billing);
        $order->addAddress($delivery);

        $form = $this->createForm(new OrdersType(), $order);

        $request = Request::createFromGlobals();
        if($request->getMethod() == "POST") {
            $form->handleRequest($request);
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($order);
                $em->flush();
                $session = new Session();
                $session->start();
                $session->getFlashBag()->add('message', 'Order successfully added');
            }
        }

        return $this->render('TestBundle:Order:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
