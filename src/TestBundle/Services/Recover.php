<?php

namespace TestBundle\Services;


use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;


class Recover
{
    private $url = null;
    private $logger = null;

    public function __construct(LoggerInterface $logger, $url)
    {
        $this->logger   = $logger;
        $this->url      = $url;
    }

    /**
     * Get all orders from xml file
     *
     * @return array
     */
    public function getOrdersFromXml()
    {
        $document = new \DOMDocument();
        $document->load($this->url);

        $crawler = new Crawler($document);
        $data = array();

        foreach ($crawler->children()->filter('orders')->children() as $key => $domElement) {
            $this->logger->debug(sprintf('get order n° "%s".', $key));
            $data[$domElement->nodeName.$key] = array();

            $data[$domElement->nodeName.$key]['info']       = $this->getOrderFromDOM($domElement);
            $data[$domElement->nodeName.$key]['billing']    = $this->getAddressFromDOM($domElement, 'billing');
            $data[$domElement->nodeName.$key]['delivery']   = $this->getAddressFromDOM($domElement, 'delivery');

            $data[$domElement->nodeName.$key]['orderLines'] = array();
            foreach ( $domElement->getElementsByTagName('cart')->item(0)->getElementsByTagName('products') as $i => $domNodeList) {
                $data[$domElement->nodeName.$key]['orderLines'][$i] = array();
                $data[$domElement->nodeName.$key]['orderLines'][$i]['orderLine']   = $this->getOrderLineFromDOM($domNodeList);
                $data[$domElement->nodeName.$key]['orderLines'][$i]['product']     = $this->getProductFromDOM($domNodeList);
            }
        }

        return $data;
    }

    /**
     * Get order from DOMElement
     *
     * @return array
     */
    public function getOrderFromDOM(\DOMElement $domElement)
    {
        $order = array();
        $order['state']     = $domElement   ->getElementsByTagName('order_status')->item(0)
                                            ->getElementsByTagName('lengow')->item(0)->nodeValue;
        $this->logger->debug(sprintf('state: "%s".',  $order['state']));
        $order['reference'] = $domElement   ->getElementsByTagName('order_id')->item(0)->nodeValue;
        $this->logger->debug(sprintf('reference: "%s".', $order['reference']));
        if($domElement   ->getElementsByTagName('order_purchase_date')->item(0)->nodeValue != '' and
            $domElement   ->getElementsByTagName('order_purchase_heure')->item(0)->nodeValue) {
            $date               = $domElement   ->getElementsByTagName('order_purchase_date')->item(0)->nodeValue .' '.
                                    $domElement   ->getElementsByTagName('order_purchase_heure')->item(0)->nodeValue ;
            $order['created']   = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
            $this->logger->debug(sprintf('date: "%s".', $date));
        }

        $order['amount']    = $domElement   ->getElementsByTagName('order_amount')->item(0)->nodeValue;
        $this->logger->debug(sprintf('amount: "%s".', $order['amount']));
        $order['tax']       = $domElement   ->getElementsByTagName('order_tax')->item(0)->nodeValue;
        $order['shipping']  = $domElement   ->getElementsByTagName('order_shipping')->item(0)->nodeValue;
        $order['commission'] = $domElement  ->getElementsByTagName('order_commission')->item(0)->nodeValue;
        $order['currency']  = $domElement   ->getElementsByTagName('order_currency')->item(0)->nodeValue;
        return $order;
    }

    /**
     * Get address("billing"/"delivery") from DOMElement
     *
     * @return array
     */
    public function getAddressFromDOM(\DOMElement $domElement, $type)
    {
        $address = array();
        $address['society']     =  $domElement  ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_society')->item(0)->nodeValue;
        $address['civility']    =  $domElement ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_civility')->item(0)->nodeValue;
        $address['lastname']    =  $domElement ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_lastname')->item(0)->nodeValue;
        $address['firstname']   =  $domElement->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_firstname')->item(0)->nodeValue;
        $address['email']       =  $domElement    ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_email')->item(0)->nodeValue;
        $address['located']     =  $domElement  ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_address')->item(0)->nodeValue;
        $address['complement']  =  $domElement->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_address_complement')->item(0)->nodeValue;
        $address['zipcode']     =  $domElement  ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_zipcode')->item(0)->nodeValue;
        $address['city']        =  $domElement     ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_city')->item(0)->nodeValue;
        $address['country']     =  $domElement  ->getElementsByTagName($type.'_address')->item(0)
                                            ->getElementsByTagName($type.'_country')->item(0)->nodeValue;
        return $address;
    }

    /**
     * Get orderLine from DOMElement
     *
     * @return array
     */
    public function getOrderLineFromDOM(\DOMElement $domNodeList)
    {
        $orderLine = array();
        $orderLine['quantity']      = $domNodeList->getElementsByTagName('quantity')->item(0)->nodeValue;
        $orderLine['price']         = $domNodeList->getElementsByTagName('price')->item(0)->nodeValue;
        $orderLine['unitPrice']     = $domNodeList->getElementsByTagName('price_unit')->item(0)->nodeValue;
        $orderLine['shippingPrice'] = $domNodeList->getElementsByTagName('shipping_price')->item(0)->nodeValue;
        $orderLine['tax']           = $domNodeList->getElementsByTagName('tax')->item(0)->nodeValue;

        return $orderLine;
    }

    /**
     * Get product from DOMElement
     *
     * @return array
     */
    public function getProductFromDOM(\DOMElement $domNodeList)
    {
        $product = array();
        $product['sku']     = $domNodeList->getElementsByTagName('sku')->item(0)->nodeValue;
        $product['ean']     = $domNodeList->getElementsByTagName('ean')->item(0)->nodeValue;
        $product['title']   = $domNodeList->getElementsByTagName('title')->item(0)->nodeValue;
        $product['image']   = $domNodeList->getElementsByTagName('url_image')->item(0)->nodeValue;

        return $product;
    }
}