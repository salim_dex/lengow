<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class OrdersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference', TextType::class)
            ->add('state', ChoiceType::class, array(
                'choices' => array(
                    'New'           => 'New',
                    'Processing'    => 'Processing',
                    'Finished'      => 'Finished',
                ),
            ))
            ->add('createDate', 'datetime')
            ->add('amount', TextType::class)
            ->add('tax', TextType::class)
            ->add('shipping', TextType::class)
            ->add('commission', TextType::class)
            ->add('currency', ChoiceType::class, array(
                'choices' => array(
                    '€'             => 'Euro',
                    '$'             => 'Dollars',
                    '£'             => 'Livre sterling',
                    '¥'             => 'Yen'
                ),
            ))
            ->add('address', CollectionType::class, array(
                    'entry_type'    => AddressType::class
            ))
            ->add('orderLines', CollectionType::class, array(
                    'entry_type'    => OrderLineType::class,
                    'allow_add'     => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Orders'
        ));
    }
}
